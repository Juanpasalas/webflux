package com.example.webflux.model;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder(toBuilder = true)
@Document(collection = "clients")
public class Client implements Serializable {

    @Id
    private String id;

    @NotEmpty
    private String name;
    @NotNull
    private Integer age;
    @NotNull
    private double salary;

    private String photo;


}
