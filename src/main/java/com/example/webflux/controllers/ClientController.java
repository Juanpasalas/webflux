package com.example.webflux.controllers;

import com.example.webflux.model.Client;
import com.example.webflux.services.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.WebExchangeBindException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.io.File;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController()
@RequestMapping("/api/clientes")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @Value("${config.uploads.path}")
    private String path;

    @PostMapping("/registerWithPhoto")
    Mono<ResponseEntity<Client>> registerWithPhoto(Client client, @RequestPart FilePart filePart) {

        convertPhoto(client, filePart);

        return filePart.transferTo(new File(path + client.getPhoto()))
                .then(clientService.save(client))
                .map(client1 -> ResponseEntity.created(URI.create("/api/clientes/".concat(client1.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(client));

    }


    @PostMapping("/upload/{id}")
    public Mono<ResponseEntity<Client>> uploadPhoto(@PathVariable String id,@RequestPart FilePart file) {

        return clientService.findById(id)
                .flatMap(client -> {
                    convertPhoto(client, file);
                    return file.transferTo(new File(path + client.getPhoto()))
                            .then(clientService.save(client));
                }).map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping
    public Mono<ResponseEntity<Flux<Client>>> findAllClients() {
        return Mono.just(
                ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(clientService.findAll())
        );
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Client>> findById(@PathVariable String id) {
        return clientService.findById(id).map(client -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(client))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping()
    public Mono<ResponseEntity<Map<String, Object>>> saveClient(@Valid @RequestBody Mono<Client> clientMono) {
        Map<String, Object> response = new HashMap<>();
        return clientMono.flatMap(client -> clientService.save(client).map(client1 -> {
            response.put("Client", client1);
            response.put("Message", "Client saved successfully");
            response.put("timestamp", new Date());
            return ResponseEntity.created(URI.create("/api/clientes/".concat(client1.getId())))
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(response);
        })).onErrorResume(throwable -> handlerException(response, throwable));
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Client>> updateClient(@RequestBody Client client, @PathVariable String id) {
        return clientService.findById(id).flatMap(client1 -> {
                    client.toBuilder().name(client1.getName());
                    client.toBuilder().age(client1.getAge());
                    client.toBuilder().salary(client1.getSalary());
                    return clientService.save(client);
                }).map(client1 -> ResponseEntity.created(URI.create("/api/clientes/".concat(client1.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(client1))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteClient(@PathVariable String id) {
        return clientService.findById(id).flatMap(client -> clientService.delete(client)
                .then(Mono.just(new ResponseEntity<Void>(HttpStatus.ACCEPTED))))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }



    private Mono<ResponseEntity<Map<String, Object>>> handlerException(Map<String, Object> response, Throwable throwable) {
        return Mono.just(throwable).cast(WebExchangeBindException.class)
                .flatMap(e -> Mono.just(e.getFieldErrors()))
                .flatMapMany(Flux::fromIterable)
                .map(fieldErrors -> "el campo " + fieldErrors.getField() + " " + fieldErrors.getDefaultMessage())
                .collectList().flatMap(strings -> {
                    response.put("Client", strings);
                    response.put("timestamp", new Date());
                    response.put("status", HttpStatus.BAD_REQUEST.value());

                    return Mono.just(ResponseEntity.badRequest().body(response));
                });
    }

    private void convertPhoto(Client client, FilePart filePart) {
        client.setPhoto(UUID.randomUUID().toString().concat("-"+filePart.filename())
                        .replace(" ", "")
                        .replace(":", "")
                        .replace("//", ""));

    }

}
